package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import database.DB_Connect;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import javax.sql.rowset.CachedRowSet;

public final class threads_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 
    DB_Connect DB = new DB_Connect();
    Connection connection = DB.establishConnection();
    Statement statement = connection.createStatement();
    Statement statement2 = connection.createStatement();
    
    int threadid = 1;
    int subcategoryid = 1;
    //int threadid = Integer.parseInt(request.getParameter("threadid"));
    
    String myQuery, myQuery2;
    ResultSet rs, rs2;
    

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"forum.css\">\n");
      out.write("\t\t<title>Vulcan Tech Forums</title>\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("\t\t<div id=\"NavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>\n");
      out.write("\t\t\t\t<li><a href=\"index.jsp\">Home</a></li>\n");
      out.write("\t\t\t\t<li class=\"active\"><a href=\"forums.jsp\">Forums</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"blogs.html\">Blogs</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"wiki.html\">Wiki</a></li>\n");
      out.write("\t\t\t\t<span id=\"signup\"><li><a href=\"signup.html\">Sign Up</a></li></span>\n");
      out.write("\t\t\t\t<span id=\"signin\"><li><a href=\"signin.html\">Sign In</a></li></span>\t\t\t\t\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div id=\"ForumNavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li>Welcome Steven</li>\n");
      out.write("\t\t\t\t<li>Notifications: <a href=\"index.jsp\">0</a></li>\n");
      out.write("\t\t\t\t<li>Post Count: <a href=\"forums.html\">0</a></li>\t\t\t\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div id=\"body\">\n");
      out.write("\t\t<div id=\"sectionHeader\">\n");
      out.write("                    \n");
      out.write("                    ");

                        myQuery = "SELECT * FROM subcategory WHERE subcategoryid='" + subcategoryid + "';";   
                        rs = statement.executeQuery(myQuery);
                    
      out.write("\n");
      out.write("                    <table>\n");
      out.write("                        <tr>\n");
      out.write("                            ");

                                rs.next();
                                out.println("<h2>" + rs.getString("title") + "</h2>");
                            
      out.write("\n");
      out.write("                        </tr>\n");
      out.write("                                            \n");
      out.write("                        <tr>\n");
      out.write("\t\t\t\t<th width=\"34%\">Thread name</th>\n");
      out.write("                \t\t<th width=\"40%\">Thread Creator</th>\n");
      out.write("\t\t\t\t<th  width=\"14%\">Last Poster</th>\n");
      out.write("\t\t\t\t<th width=\"15%\">View Count</th>\n");
      out.write("                        </tr>\n");
      out.write("                                           \n");
      out.write("                            ");

                                myQuery = "SELECT * FROM threads INNER JOIN users ON threads.lastposter=users.userid WHERE subcategoryid='" + subcategoryid + "';";   
                                myQuery2 = "SELECT * FROM threads INNER JOIN users ON threads.threadcreator=users.userid WHERE subcategoryid='" + subcategoryid + "';";   

                                rs = statement.executeQuery(myQuery);
                                rs2 = statement2.executeQuery(myQuery2);
                                            
                                while(rs.next() && rs2.next()){
                                    //rs2.next();
                                    out.println("<tr><td><a href=\"post_structure.jsp/?c=" + rs.getString("threadid") + "\">" + rs.getString("threadname") + "</td>");
                                    out.println("<td><a href=\"user.jsp/?c=\"" + rs.getInt("userid") + "\">" +  rs2.getString("username") + "</td>");
                                    out.println("<td><a href=\"user.jsp/?c=\"" + rs.getInt("userid") + "\">" + rs.getString("username") + "</td>");
                                    out.println("<td>" + rs.getInt("viewcount") + "</td>");
                                }
                            
      out.write("\n");
      out.write("\t\t\t\t\t\n");
      out.write("                    </table>\n");
      out.write("\n");
      out.write("                                        <div id=\"newthreadbutton\">\n");
      out.write("                                            </br>\n");
      out.write("                                            <form action=\"newthread.jsp\">\n");
      out.write("                                                <input type=\"submit\" value=\"Create new thread\" />\n");
      out.write("                                            </form>\n");
      out.write("                                            \n");
      out.write("                                        </div>\n");
      out.write("                                   \n");
      out.write("\t\t</div>\n");
      out.write("\t\t</div>\t\t\n");
      out.write("\t</body>\n");
      out.write("</html>\n");
      out.write("\n");

    rs.close();
    rs2.close();
    connection.close();

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
