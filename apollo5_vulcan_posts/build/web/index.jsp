<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<% 
    String driver = "com.mysql.jdbc.Driver";
    Class.forName(driver);

    String dbURL =  "jdbc:mysql://localhost:3306/apollo5_vulcan";
    String username = "root";
    String password = "";
    Connection connection = DriverManager.getConnection(dbURL, username, password);
    Statement statement = connection.createStatement();
    String myQuery;
    ResultSet rs;

    //just give it tmp data for now
    //TODO get rid of this and use one join
    String[] b = {"1","2","3","4","5","6","7","8","9","10"};
    String[] u = {"1","2","3","4","5","6","7","8","9","10"};
    %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Vulcan Test</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="main.css">
        <meta name="viewport" content="width=device-width">
    </head> 
        <div id="NavBar">
            <ul>
                <li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
		<li class="active"><a href="index.jsp">Home</a></li>
		<li><a href="forum.jsp">Forums</a></li>
		<li><a href="blogs.html">Blogs</a></li>
		<li><a href="wiki.html">Wiki</a></li>
		<span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
		<span id="signin"><li><a href="signin.html">Sign In</a></li></span>		
            </ul>	
        </div>
        
        <div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Category</h2>
				<ul>
				 <%
                                    myQuery = "SELECT * FROM category";
                                    rs = statement.executeQuery(myQuery);
                                    while(rs.next()){
                                        out.println("<li><a href=\"forums.jsp?c=" +rs.getString("categoryid") +"\">" +rs.getString("name") +"</a></li>");
                                    }
                                %>
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
                                    <h2>News content</h2>
                                    <p>Blogs and wikis are not a main priority so they may not be completed. We may switch this page with forums and make the forums the homepage, this news section will cover over news for anything tech related that every user might enjoy, not tailored for specific categories. For example, every may be interested in the note 7 catching on fire so that would be approriate news for here, however something like a new World of Warcraft raid being released might not be interesting for everyone so it wouldn't appear here. This page is essentially a moderated front page of reddit, subforums are basically subreddits. We will be using the flat forum structure. </p>
				</div>
			</div>
			<div id="content-aside">
				<div class="inner">
					<h2>Latest Posts</h2> 
					<%
                                            myQuery = "SELECT * FROM posts ORDER BY posttime DESC LIMIT 10;";
                                            
                                            myQuery = "SELECT * FROM posts ORDER BY posttime DESC LIMIT 10;";
                                            rs = statement.executeQuery(myQuery);
                                            for(int i = 0; i < 10; i++){
                                                rs.next();
                                                b[i] = rs.getString("threadid");
                                                u[i] = rs.getString("posterid");
                                            }
                                            for(int i = 0; i < 10; i++){
                                                
                                                myQuery = "SELECT threads.threadname, users.userid, users.username FROM threads INNER JOIN users ON users.userid='" + u[i] +"';";
                                                rs = statement.executeQuery(myQuery);
                                                rs.next();
                                                out.println("<p><a href=\"thread.jsp?c=" + b[i] +"\">" +rs.getString("threadname") +"</a> Posted by: <a href=\"user.jsp?c=" + u[i] +"\">" +rs.getString("username") +"</a></p><br>");
                                            }
                                        %>
				</div>
			</div>			
		</div>
		</div>	
    <br><br><br>    
</html>

<%
    rs.close();
    connection.close();
%>
