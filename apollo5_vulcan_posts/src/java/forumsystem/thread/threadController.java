/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.thread;

import forumsystem.post.Post;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Steven
 */
public class threadController extends HttpServlet {

    private int posterid;
    private int threadid;
    private int categoryid;
    private int subcategoryid;
  
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

            Threads threads = new Threads();
            threads.setLastposter(Integer.parseInt(request.getParameter("userid")));
            threads.setThreadcreator(Integer.parseInt(request.getParameter("userid")));
            threads.setCategoryid(Integer.parseInt(request.getParameter("categoryid")));
            threads.setSubcategoryid(Integer.parseInt(request.getParameter("subcategoryid")));
            threads.setViewcount(0);
            threads.setPostcount(1);
            threads.setThreadname(request.getParameter("threadnametext"));
            threads.addThread();

            Post post = new Post();
            post.setPostcontents(request.getParameter("threadposttext"));
            post.setPosterid(Integer.parseInt(request.getParameter("userid")));
            post.setpagenum(request.getParameter("pageide"));
            int threadnumber = post.insertThreadPost();
            
            //response.encodeRedirectURL("threadsuccess.jsp");
            response.sendRedirect("threadsuccess.jsp?threadid=" + threadnumber + "&pageid=" + request.getParameter("pageid"));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
     
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
