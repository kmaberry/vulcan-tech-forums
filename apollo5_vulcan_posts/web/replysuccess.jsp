<%-- 
    Document   : replysuccess
    Created on : Nov 30, 2016, 4:07:56 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%String e = request.getParameter("threadid");%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" type="text/css" href="main.css">
        <title>Confirmation</title>
    </head>
    <body>
        
        <div id="NavBar">
            <ul>
                <li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
		<li><a href="index.jsp">Home</a></li>
		<li class="active"><a href="forum.jsp">Forums</a></li>
                <li><a href="blogs.html">Blogs</a></li>
                <li><a href="wiki.html">Wiki</a></li>
                <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
		<span id="signin"><li><a href="signin.html">Sign In</a></li></span>	
            </ul>	
        </div>
        
        </br> </br>
        <h1>Reply successfully processed!</h1>
    </body>
    <%
        //String url = "post_structure.jsp?threadid=" + request.getParameter("threadid") + "&pageid=1";
    
        //out.println(url);
        %>
    <script>
        setTimeout(function() {
        //window.location.href = url;
        //window.location.href = "threads.jsp";
        document.location = "threads.jsp?"; //I could not figure out how to get the page redirection to work right
        //If it would have worked right, it would have brought the user back to the page of which they clicked reply

        }, 4000);
    </script>
</html>
