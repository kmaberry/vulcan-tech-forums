<%-- 
    Document   : manage
    Created on : Dec 5, 2016, 8:16:47 PM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/forum_manage.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
                        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li class="active"><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"><li ><a href="${pageContext.request.contextPath}/user?uname=<%=name%>"><% out.print(name); %></a></li></span>
              
        <%}  
        else{ 
        %>
        <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>                               
				
								
			</ul>	
		</div>
		
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Management</h2>
				<ul>
				<li class="active"><a href="manage.jsp">Category Lists</a></li>
				<li><a href="new_category.html">Add New Category</a></li>				
				<li><a href="${pageContext.request.contextPath}/usermanage">User Management</a></li>
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
					<table>
					<tr>
						<h2>General Discussion</h2>
					</tr>
					<tr>
						<th width="34%">Subsection name</th>
						<th width="40%">Recent thread </th>
						<th  width="14%">Post count</th>
						<th width="15%">Thread count</th>
						<th width="15%">Manage</th>
					</tr>
					<tr>
						<td><a href="text-edit.html">Text Editors<a></td>
						<td><a href="post_structure.html">Is vim or emacs better</a></td>
						<td>1,000,000</td>
						<td>2</td>
						<td><input type="button" value="Edit"></td>
					</tr>
					</table>

					<table>
					<tr>
						<h2>Not so general discussion</h2>
					</tr>
					<tr>
						<th width="34%">Subsection name</th>
						<th width="40%">Recent thread </th>
						<th  width="14%">Post count</th>
						<th width="15%">Thread count</th>
						<th width="15%">Manage</th>
					</tr>
					<tr>
						<td><a href="pixl.html">Google Pixl</a></td>
						<td><a href = "pixl_thread.html">how do i turn my phone on? please help </a></td>
						<td>506</td> 
						<td>50</td>
						<td><input type="button" value="Edit"></td>
					</tr>					
					</table>
				</div>
			</div>					
		</div>
		</div>		
	</body>
</html>
