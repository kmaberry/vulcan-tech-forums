<%-- 
    Document   : viewprofile
    Created on : Dec 5, 2016, 6:25:14 PM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/post_structure.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
                                <li><a href="forums.jsp">Forums</a></li>
                                <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <li class="active"><a href="${pageContext.request.contextPath}/user?uname=<%=name%>">User Profile</a></li>
        <span style="float:right;"><li ><a href="${pageContext.request.contextPath}/user?uname=<%=name%>"><% out.print(name); %></a></li></span>
              
        <%}  
        else{ 
        %>
        <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
                                
								
			</ul>	
		</div>
		
		<br><br>
		<div id="body">

		<div id="post">
                    <h2><%=request.getAttribute("uname")%></h2>
			<div id="userInfo">
				<img src="<%=request.getAttribute("profilepic")%>" alt="Someone" height="60" width="60" align="center">
				<p><i><%=request.getAttribute("uname")%></i></p>
					<div id="UInfo">
						<p>Post Count: <%=request.getAttribute("postcount")%> </p>
						<p>Date joined: <%=request.getAttribute("datejoined")%></p>
						<p>Profile Views: 10</p>
					</div>	
			</div>	
				<h3>About Me</h3>
				<p><%=request.getAttribute("about")%></p>
				<h3>Interests</h3>
				<p><%=request.getAttribute("interests")%></p>
				<br>
                                <span style="float:right">
                                    <p><a href="${pageContext.request.contextPath}/user?edit=1&uname=<%=name%>">Edit Profile</a>
                                    </p>
                                </span>
		</div>
		
		</div>		
	</body>
</html>
