<%-- 
    Document   : home
    Created on : Dec 3, 2016, 8:50:58 PM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/forum.css">
	<title>Vulcan Tech Forums</title>
    </head>
    <body>
        
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li class="active"><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"><li ><a href="${pageContext.request.contextPath}/user?uname=<%=name%>"><% out.print(name); %></a></li></span>
              
        <%}  
        else{ 
        %>
        <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
                </ul>	
		</div>		
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Category</h2>
				<ul>
				<li><a href="forums.html#gendisc">General Discussion</a></li>
				<li><a href="forums.html#mobdev">Mobile</a></li>
				<li><a href="#.html">...</a></li>
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
					<h2>News content</h2>
					<p>Blogs and wikis are not a main priority so they may not be completed. We may switch this page with forums and make the forums the homepage, this news section will cover over news for anything tech related that every user might enjoy, not tailored for specific categories. For example, every may be interested in the note 7 catching on fire so that would be approriate news for here, however something like a new World of Warcraft raid being released might not be interesting for everyone so it wouldn't appear here. This page is essentially a moderated front page of reddit, subforums are basically subreddits. We will be using the flat forum structure. </p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
				</div>
			</div>
			<div id="content-aside">
				<div class="inner">
					<h2>Latest Posts</h2> 
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
				</div>
			</div>			
		</div>
		</div>	
    </body>
</html>
