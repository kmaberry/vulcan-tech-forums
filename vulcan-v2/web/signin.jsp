<%-- 
    Document   : login
    Created on : Nov 30, 2016, 11:19:43 AM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vulcan Tech Forums - sign in</title>
	<link rel="stylesheet" type="text/css" href="css/signin.css">
    </head>
    <body>
        <div id="welcome"> 
		<h2>Welcome to Vulcan Tech Forums</h2>
	</div>
	<div id="form">
	<form action="signin" method="post">            
            <table align="center">				
                <tr>
                    <td>Username: </td>
                    <td><input required type="text" id="uname" name="uname" maxlength="15" value="<%=request.getParameter("uname")%>"></td>
                </tr>
				<tr></tr>
                <tr>
                    <td>Password: </td>
                    <td><input required type="password" id="pwd" name="pwd" maxlength="20"></td>
                </tr>
				<tr></tr>
				<tr>
                    <td></td>
                    <td><input type="submit" value="Log In"></td>
                </tr>
				<tr>
				</tr>
				<tr>
                    <td></td>
                    <td><a href="forgotpw.html">Forgot Password?</a></td>
                </tr>                
            </table><br/>
            <center><span style="color: red;">Error: <%=request.getAttribute("error")%></span></center>
        </form>
	</div>
    </body>
</html>
