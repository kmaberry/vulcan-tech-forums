<%-- 
    Document   : replysuccess
    Created on : Nov 30, 2016, 4:07:56 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="database.*;" %>
<%@page import="java.sql.*;"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="main.css">
        <title>JSP Page</title>
    </head>
    <body>
        
        <div id="NavBar">
            <ul>
                <li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
		<li><a href="index.html">Home</a></li>
		<li class="active"><a href="forums.html">Forums</a></li>
                <li><a href="blogs.html">Blogs</a></li>
                <li><a href="wiki.html">Wiki</a></li>
                <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
		<span id="signin"><li><a href="signin.html">Sign In</a></li></span>	
            </ul>	
        </div>
        
        </br> </br>
        <h1>Reply successfully processed!</h1>
    </body>
    <script>
        setTimeout(function() {
        window.location.href = "index.html";
        }, 4000);
    </script>
</html>
