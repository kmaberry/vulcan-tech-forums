/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.post;

import database.DB_Connect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.logging.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Steven
 */
public class Post {
    
    private int postid;
    private int threadid;
    private int posterid;
    private String timestamp;
    private String postcontents;
 
    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public int getThreadid() {
        return threadid;
    }

    public void setThreadid(int threadid) {
        this.threadid = threadid;
    }

    public int getPosterid() {
        return posterid;
    }

    public void setPosterid(int posterid) {
        this.posterid = posterid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPostcontents() {
        return postcontents;
    }

    public void setPostcontents(String postcontents) {
        this.postcontents = postcontents;
    }
    
    public void insertPost(HttpServletRequest request, HttpServletResponse response) {
        
        try{
            PrintWriter out = response.getWriter();
        
            try{
                
                DB_Connect DB = new DB_Connect();
                Connection DB_connection = DB.establishConnection();

                //int posterid = 1; //temporary until we figure out the user system        
                String query = "INSERT INTO posts (threadid, posterid, postcontents) " + "VALUES ('"+this.threadid+"', '"+this.posterid+"', '"+this.postcontents+"')";
                //out.println("</br>Query insertpost(): </br>" + query);

                Statement myStatement = DB_connection.createStatement();  

                try{
                    int executeUpdate = myStatement.executeUpdate(query);
                    myStatement.close();
                    DB_connection.close();
                    response.sendRedirect("replysuccess.jsp");
                    out.flush();
                }catch(SQLException err){
                    out.println("SOME ERROR OCCURED: " + err);
                }

                //more things...
               // window.location = "http://www.yoururl.com";
               

            }catch(SQLException | NullPointerException err){
                out.println("SOME ERROR OCCURED: " + err);
            }
        }catch(IOException err){
            err.printStackTrace();
        }
    }
    
    /*Used to reteieve post made by user*/
    public void retrievePost(HttpServletRequest request, HttpServletResponse response){
        
        try{
            DB_Connect DB = new DB_Connect();
            Connection DB_connection = DB.establishConnection();
            
            PrintWriter out = response.getWriter();
            
            String query = "SELECT * FROM posts WHERE posterid = " + this.posterid;
            //out.println("Query is " + query);
            
            Statement myStatement = DB_connection.createStatement();
            
            ResultSet rs = myStatement.executeQuery(query);
            
            while(rs.next()){
         
                out.printf("User ID = " + this.posterid);
                out.printf(" --> " + rs.getString("postcontents") + "</br>");
                //out.printf("");
            }   
            out.flush();
            rs.close();
            DB_connection.close();
            
        }catch(SQLException | IOException err){
            err.printStackTrace();
            Logger.getLogger(Post.class.getName()).log(Level.SEVERE, null, err);
        }
    }    
}
