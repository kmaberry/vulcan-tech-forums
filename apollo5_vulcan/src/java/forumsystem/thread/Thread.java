/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.thread;

import database.DB_Connect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Steven
 */
public class Thread {
    
    private int threadid;
    private String threadname;
    private int lastposter;
    private int postcount;
    private int threadcreator;
    private int viewcount;
    private int categoryid;
    private int subcategoryid; 

    public int getThreadid() {
        return threadid;
    }

    public void setThreadid(int threadid) {
        this.threadid = threadid;
    }

    public String getThreadname() {
        return threadname;
    }

    public void setThreadname(String threadname) {
        this.threadname = threadname;
    }

    public int getLastposter() {
        return lastposter;
    }

    public void setLastposter(int lastposter) {
        this.lastposter = lastposter;
    }

    public int getPostcount() {
        return postcount;
    }

    public void setPostcount(int postcount) {
        this.postcount = postcount;
    }

    public int getThreadcreator() {
        return threadcreator;
    }

    public void setThreadcreator(int threadcreator) {
        this.threadcreator = threadcreator;
    }

    public int getViewcount() {
        return viewcount;
    }

    public void setViewcount(int viewcount) {
        this.viewcount = viewcount;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public int getSubcategoryid() {
        return subcategoryid;
    }

    public void setSubcategoryid(int subcategoryid) {
        this.subcategoryid = subcategoryid;
    }
    
      public void addThread(HttpServletRequest request, HttpServletResponse response) {
        
        try{
            PrintWriter out = response.getWriter();
        
            try{
                
                DB_Connect DB = new DB_Connect();
                Connection DB_connection = DB.establishConnection();

                //int posterid = 1; //temporary until we figure out the user system        
                String query = "INSERT INTO threads (threadid, threadname, lastposter, threadcreator, viewcount, categoryid, subcategoryid) " + "VALUES ('"+this.threadid+"', '"+this.threadname+"', '"+this.lastposter+"',  '"+this.categoryid+"',  '"+this.subcategoryid+"'  )";
                //out.println("</br>Query insertpost(): </br>" + query);

                Statement myStatement = DB_connection.createStatement();  

                try{
                    int executeUpdate = myStatement.executeUpdate(query);
                    myStatement.close();
                    DB_connection.close();
                    response.sendRedirect("threadsuccess.jsp");
                    out.flush();
                }catch(SQLException err){
                    out.println("SOME ERROR OCCURED: " + err);
                }

                //more things...
               // window.location = "http://www.yoururl.com";
               

            }catch(SQLException | NullPointerException err){
                out.println("SOME ERROR OCCURED: " + err);
            }
        }catch(IOException err){
            err.printStackTrace();
        }
    }
    
}
