/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
import java.util.logging.*;

/**
 *
 * @author Steven
 */
public class DB_Connect {
    
    public Connection establishConnection(){
        
        try{
            String driver = "com.mysql.jdbc.Driver", username = "apollo5", password = "Vulcanforums";
            Class.forName(driver);
            
            String dbURL = "jdbc:mysql://localhost:3306/apollo5_vulcan";
            Connection connect = DriverManager.getConnection(dbURL, username, password);
        
            System.out.println("Connection Successful");
            
            return connect;
            
        }catch(ClassNotFoundException | SQLException err){
            System.out.println("NO CONNECTION");
            
        }
        return null;
    }  
}