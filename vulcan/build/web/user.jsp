<%-- 
    Document   : viewprofile
    Created on : Dec 5, 2016, 6:25:14 PM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/post_structure.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
            <% String uname=(String)request.getParameter("uname"); %>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
                                <li><a href="forums.jsp">Forums</a></li>
				<li class="active"><a href="user.jsp">User Profile</a></li>				
			</ul>	
		</div>
		
		<br><br>
		<div id="body">

		<div id="post">
                    <h2><a href="#"><%=request.getAttribute("uname")%></a></h2>
			<div id="userInfo">
				<img src="<%=request.getAttribute("profilepic")%>" alt="Someone" height="60" width="60" align="center">
				<p><i><a href="#"><%=request.getAttribute("uname")%></a></i></p>
					<div id="UInfo">
						<p>Post Count: <%=request.getAttribute("postcount")%> </p>
						<p>Date joined: <%=request.getAttribute("datejoined")%></p>
						<p>Profile Views: 10</p>
					</div>	
			</div>	
				<h3>About Me</h3>
				<p><%=request.getAttribute("about")%></p>
				<h3>Interests</h3>
				<p><%=request.getAttribute("interests")%></p>
				<br>
		</div>
		
		</div>		
	</body>
</html>
