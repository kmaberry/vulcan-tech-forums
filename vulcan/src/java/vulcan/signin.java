/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vulcan;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author SUN
 */
public class signin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String uname = request.getParameter("uname");
            String pwd = request.getParameter("pwd");
            try {
            DB_Con DB_C = new DB_Con();
            Connection con = DB_C.DB_Con();
            Statement statement = con.createStatement();
            String myQuery = "SELECT username, hashedpass, usergroup FROM users where username='"+uname+"';"; 
            ResultSet rs = statement.executeQuery(myQuery);
            String url = null;
            String error = null;
            HttpSession session=request.getSession();
            if (rs.next()==false){
                request.setAttribute("error", "username does not exist !!!");
                request.getRequestDispatcher("signin.jsp").forward(request, response);
                url = "/signin.jsp";                
            }
            else {
                if(pwd.equals(rs.getString("hashedpass"))){                      
                    session.setAttribute("username",uname); 
                    session.setAttribute("usergroup", rs.getString("usergroup"));
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                    url = "/index.jsp"; 
                }
                else{
                    request.setAttribute("error", "wrong password !!!");
                    request.getRequestDispatcher("signin.jsp").forward(request, response);
                    url = "/signin.jsp";                    
                }
            }
            rs.close();
            statement.close();
            con.close();
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request, response);
            } catch (Exception e) {
            System.err.println("Error with connection: " + e);
            }            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
