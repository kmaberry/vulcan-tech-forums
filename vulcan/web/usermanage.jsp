<%-- 
    Document   : usermanage
    Created on : Dec 5, 2016, 8:04:03 PM
    Author     : SUN
--%>

<%@page import="java.util.*"%>
<%@page import="vulcan.userattr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/forum_manage.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a>				
				<li><a href="forums.jsp">Forums</a></li>									
			</ul>	
		</div>
		
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Management</h2>
				<ul>
				<li><a href="forum_manage.html">Category Lists</a></li>
				<li><a href="new_category.html">Add New Category</a></li>				
				<li class="active"><a href="${pageContext.request.contextPath}/usermanage">User Management</a></li>
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
					<table>
					<tr>
						<h2>User Management</h2>
					</tr>
					<tr>
						<th width="20%">user name</th>
						<th width="20%">user email</th>						
						<th width="20%">user roals </th>
                                                <th width="20%">user status </th>
						<th width="20%">operation </th>
					</tr>
                                        <% 
                                        String group = "";
                                        String status = "";
                                        ArrayList<userattr> ulist=(ArrayList<userattr>)request.getAttribute("userlist");
                                        for(userattr u: ulist){
                                        %>
					<tr>
						<td width="20%"><a href="${pageContext.request.contextPath}/user?uname=<%=u.uname%>"><%=u.uname%></a></td>
						<td width="20%"><%=u.email%></td>						
						<td width="20%">
                                                    <% if(u.groupid==3) group = "Administrator";
                                                    else if(u.groupid==2) group = "Moderator";
                                                    else group = "Ordinary";
                                                    %>
                                                    <%=group%>
                                                </td>
                                                <td width="20%"><% if(u.status==0) status = "Normal";
                                                    else status = "isBanned";%>
                                                    <%=status%>
                                                </td>
                                                <td width="20%"><% if(u.status!=1){%>
                                                    <form action="banuser"><input type="submit" value="ban"></form> 
                                                    <%}else{%>
                                                    <form action="unbanuser"><input type="submit" value="unban"></form>
                                                    <%}%>
                                                </td>
					</tr>
                                        <%}%>
					</table>
				</div>
			</div>					
		</div>
		</div>		
	</body>
</html>
