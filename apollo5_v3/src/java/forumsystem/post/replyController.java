/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.post;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet controls how replies are inserted into the database
 * @author Steven
 */
@WebServlet(name = "replyController", urlPatterns = {"/replyController"})
public class replyController extends HttpServlet {
    
    private int posterid = 1;
    private int threadid = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NullPointerException {   
        
        //PrintWriter out = response.getWriter();
        //out.println(request.getParameter("posterid"));
        
        this.threadid = Integer.parseInt(request.getParameter("threadid")); 
        this.posterid = Integer.parseInt(request.getParameter("posterid"));
        String pages = request.getParameter("pageide");
        
        Post post = new Post();
        post.setPosterid(posterid);
        post.setpagenum(pages);
            
        post.setThreadid(threadid);
        post.setPostcontents(request.getParameter("replycontenttext"));
        post.insertPost(request, response);
        
        //post.retrievePost(request,response); //was used for testing        
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {  
        processRequest(request, response);  
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NullPointerException  {
        
        processRequest(request, response);
        PrintWriter out = response.getWriter();
   
        //When user interface and category interface is finished, apply them here
        //int posterid = 1, threadid = 1; //set by default
        
        //HttpSession session = request.getSession();
       
        
     
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
