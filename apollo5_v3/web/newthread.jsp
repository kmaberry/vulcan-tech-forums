<%-- 
    Document   : newreply
    Created on : Nov 28, 2016, 6:37:25 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.servlet.*"%>
<%@page import="database.*"%>
<%@page import="postsystem.*"%>

<%
    String userid = request.getParameter("userid");
    String categoryid = request.getParameter("categoryid");
    String subcategoryid = request.getParameter("subcategoryid");
    String threadid = request.getParameter("threadid");
    %>

<!DOCTYPE html>
<html>
	<head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="main.css">
		<link rel="stylesheet" type="text/css" href="newreply.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li class="active"><a href="forum.jsp">Forums</a></li>
				<span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
				<span id="signin"><li><a href="signin.jsp">Sign In</a></li></span>	
			</ul>	
		</div>
            </div>
            
            <script>
                function checkform() { //KEEP USER FROM SUBMITTING BLANK REPLIES
                    if(document.getElementById("threadposttext").value === ''){
                            if(document.getElementById("threadnametext").value === '') {
                                alert ('Thread name and intial post content required');
                                return false;
                            }
                         alert ('intial post content required');                       
                        return false;
                    } else {
                        document.replycontents.submit(); 
                    }
                }
            </script>
            
		<div id="addreplybody">
                    <h1>New Thread<h1/>
                        
                        <p><font size="3">Thread Title</font></p>
                        <form onsubmit="return checkform()" action="threadController" name="threadnametext" method="post">
                            <textarea id="threadnametext" class="text" cols="86" rows ="2" name="threadnametext"></textarea>
                        
                        
                            <p><font size="3">Initial Post Contents</font></p>
                        
                            <textarea id="threadposttext" class="text" cols="86" rows ="10" name="threadposttext"></textarea>
                            </br>
                            <input type="hidden" name="userid" value="<%=userid%>">
                            <input type="hidden" name="subcategoryid" value="<%=subcategoryid%>">
                            <input type="hidden" name="categoryid" value="<%=categoryid%>">
                            <input type="hidden" name="threadid" value="<%=threadid%>">
                            <input type="hidden" name="threadname" value="threadnametext">
                            <input type="hidden" name="threadposttext" value="threadposttext">
                            <input type="submit" value="Submit Thread" class="submitButton">   
                        </form>	 
		</div>	       
	</body>
</html>
