<%-- 
    Document   : threadsuccess
    Created on : Dec 1, 2016, 8:33:52 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="main.css">
        <title>Confirmation</title>
    </head>
    <body>
        
        <div id="NavBar">
            <ul>
                <li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
		<li><a href="index.jsp">Home</a></li>
		<li class="active"><a href="forum.jsp">Forums</a></li>
                <li><a href="blogs.html">Blogs</a></li>
                <li><a href="wiki.html">Wiki</a></li>
                <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
		<span id="signin"><li><a href="signin.html">Sign In</a></li></span>	
            </ul>	
        </div>
        
        </br> </br>
        <h1>Thread successfully processed!</h1></br>
        <p>Go ahead and select your thread from the threads page</p></br>
    </body>
    <script>
        setTimeout(function() {
            window.location.href = "threads.jsp";
        }, 4000);
    </script>
</html>
