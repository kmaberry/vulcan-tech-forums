<%-- 
    Document   : post_structure
    Created on : Dec 5, 2016, 10:18:55 AM
    Author     : Steven
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="database.DB_Connect"%>
<%@page import="java.sql.Connection, java.sql.SQLException, java.sql.Statement, java.sql.ResultSet, java.sql.DriverManager"%>
<%@ page import="javax.sql.rowset.CachedRowSet"%>
<%@ page import="java.util.*"%>  
<%@ page import="forumsystem.post.Postfetch"%>
<%@ page import="forumsystem.post.postpage"%>

<% 
    DB_Connect DB = new DB_Connect();
    Connection connection = DB.establishConnection();
    Statement statement = connection.createStatement();
    
    String threadid = request.getParameter("threadid"), myQuery, myQuery2, post_time, myTimeStamp;
    int pageid = Integer.parseInt(request.getParameter("pageid")), temppage = Integer.parseInt(request.getParameter("pageid"));
    int perpage = 10; //amount of post per page to display
    
    int posterid = 1; //THIS MANAGES WHO IS POSTING
    
    if(pageid == 1){
        //NO action
    }
    else if(pageid == 0 || pageid < 0){
        temppage = 1; //PREVENT EXCEPTIONS
        pageid = 1;
    }
    else{
        pageid = pageid - 1;
        pageid = pageid * perpage + 1;
    }
    
    List<Postfetch> postlist = postpage.getPost(pageid, perpage, threadid); 
    ResultSet rs, rs2; 
    DateFormat format;
    
    int userid = 1;   //Assumes admin
%>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="post_structure.css">
		<link rel="stylesheet" type="text/css" href="forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
                            <li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
                            <li><a href="index.jsp">Home</a></li>
                            <li class="active"><a href="forum.jsp">Forums</a></li>
                            <span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
                            <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
			</ul>	
		</div>
		<!--<div id="ForumNavBar">
			<ul>
				<li>Welcome Anonymous</li>
				<li>Notifications: <a href="index.html">0</a></li>
				<li>New Posts: <a href="forums.html">0</a></li>				
			</ul>	
		</div>-->
            
		<div id="body">
                    <%  
                        myQuery = "SELECT * FROM threads WHERE threadid=" + threadid;   
                        rs = statement.executeQuery(myQuery);
                        rs.next();
                        
                        /*This entire thing lets you go back and forth between pages (bug problem, pageid will go below 0 and beyond amount of post)*/
                        out.println("<h2>" + rs.getString("threadname") + "<br>" + //thread name
                                "<a href=\"post_structure.jsp?threadid=" + threadid + "&pageid=" + //left arrow (previous page)
                                (Integer.parseInt(request.getParameter("pageid")) - 1) + "\"> << </a>Page: " + temppage + 
                                "<a href=\"post_structure.jsp?threadid=" + threadid + "&pageid=" + //Right arrow (next page)
                                (Integer.parseInt(request.getParameter("pageid")) + 1) + "\"> >> </a></h2>");

                        myQuery2 = "SELECT * FROM posts INNER JOIN users ON posts.posterid=users.userid WHERE threadid=" + threadid;
                        rs = statement.executeQuery(myQuery2);
     
                        /*everything commented out will display the all post on one page*/
                        /*postlist contains N posts (=perpage) of which to display on the page*/
                        for(Postfetch list:postlist){

                            out.println("<div id=\"post\">" + "<div id=\"userInfo\">");// start userinfo
                                    
                            //User Profile Picture
                            //out.println("<img src=\"" + rs.getString("profilepicture") + "\"alt=\"Your face here\" height=\"60\" width=\"60\" align=\"center\">");
                            out.println("<br><img src=\"" + list.getProfilePicture() + "\"alt=\"Your face here\" height=\"65\" width=\"65\" align=\"center\">");
     
                            //THIS IS HOW A USER PROFILE PAGE IS REFERENCED, IDK THE NAME OF THE FILE OR HOW IT ACCEPTS THINGS
                            //User name and link to page
                            //out.println("<p><i><a href=\"user.jsp/?userid=" + rs.getInt("userid") + "\">" +  rs.getString("username") + "</a></i></p>");
                            out.println("<p><i><a href=\"user.jsp/?userid=" + list.getPosterid() + "\">" +  list.getUsername() + "</a></i></p>");
       
                            //Post Time
                            //myTimeStamp = rs.getString("posttime");
                                    
                            //start uinfo
                            out.println("<div id =\"UInfo\"><p> Posted: ");
                            out.println(list.gettimeStamp() + "</div>");
                            //out.println(myTimeStamp + "</div>");
                            out.println("</div>"); //close userinfo
                            //End uinfo

                            //start communication
                            out.println("<div id=\"communication\"> <br>");
                            //out.println("<button type=\"button\" onclick=\"window.location.href='newreply.jsp?threadid=" + threadid + "&userid=" + userid + "'\">Quote</button>");
//Quote feature on hold     //out.println("<button type=\"button\" onclick=\"window.location.href='newreply.jsp?threadid=" + list.getThreadid() + "&userid=" + list.getUsername() + "'\">Quote</button>");
                            out.println("<button type=\"button\" onclick=\"window.location.href='newreply.jsp?threadid=" + list.getThreadid() + "&pageident=" + temppage + "'\">Reply</button>");

                            //out.println("<button type=\"button\" onclick=\"window.location.href='newreply.jsp?threadid=" + threadid + "&userid=" + userid + "'\">Reply</button>");
                            out.println("</div>");
                            //end communication
                                    
                            //start postelement
                            out.println("<div id=\"postelement\">");
                            //out.println("<p>" + rs.getString("postcontents") + "</p><br></div>");
                            out.println("<p>" + list.getPostcontents() + "</p><br></div>");
                            //end postelement
                                    
                            out.println("</div>");
                        }
                        
                        session.setAttribute("userid", posterid); //pass userid without url
                        session.setAttribute("pageident", temppage);
                        
                        myQuery = "UPDATE threads SET viewcount = viewcount + 1 WHERE threadid=" + threadid; 
                        statement.executeUpdate(myQuery);
                %>	
	</body>
</html>

<%
    rs.close();
    connection.close();
%>