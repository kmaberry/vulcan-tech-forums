<%-- 
    Document   : threads
    Created on : Dec 1, 2016, 11:19:13 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="database.DB_Connect"%>
<%@page import="java.sql.Connection, java.sql.SQLException, java.sql.Statement, java.sql.ResultSet, java.sql.DriverManager"%>
<%@ page import="javax.sql.rowset.CachedRowSet"%>

<% 
    DB_Connect DB = new DB_Connect();
    Connection connection = DB.establishConnection();
    Statement statement = connection.createStatement();
    Statement statement2 = connection.createStatement();

    int threadid = 1; //SINCE I DON'T KNOW HOW THINGS WILL BE PASSED TO THIS PAGE, EVERYTHING IS SET TO 1 FOR NOW
    int subcategoryid = 1;
    
    int userid = 1; //IF THIS IS SET, IT IS PASSED ON WITH POST FROM HERE ON TO KEEP FROM BEING ADDED TO URL...
    //THIS WOULD SIGNIFIY THE SIGNED IN USER
    
    int categoryid = 1;
    
    //THESE WOULD WORK IF THEY ARE PASSED BY GET OR POST
    //int subcategoryid = Integer.pareseInt(request.getParameter("subcategoryid"));
    //int threadid = Integer.pareseInt(request.getParameter("threadid"));
    //int userid = Integer.pareseInt(request.getParameter("userid"))
    //int categoryid = Integer.parseInt(getParameter("categoryid"));
    
    String myQuery, myQuery2;
    ResultSet rs, rs2;  
%>

<!DOCTYPE html>
<html>
	<head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="main.css">
		<link rel="stylesheet" type="text/css" href="forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li class="active"><a href="forum.jsp">Forums</a></li>
				
				<span id="signup"><li><a href="signup.html">Sign Up</a></li></span>
				<span id="signin"><li><a href="signin.html">Sign In</a></li></span>					
			</ul>	
		</div>
		<!--<div id="ForumNavBar">
			<ul>
				<li>Welcome Steven</li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<li>Post Count: <a href="forums.html">0</a></li>				
			</ul>	
		</div>-->
		<div id="body">
		<div id="sectionHeader">  
                    <%
                        myQuery = "SELECT * FROM subcategory WHERE subcategoryid='" + subcategoryid + "';";   
                        rs = statement.executeQuery(myQuery);
                    %>     
                    <table>
                        <tr>
                        <%
                            rs.next();
                            out.println("<h2>" + rs.getString("title") + "</h2>");
                        %>
                        </tr>                       
                        <tr>
                            <th width="34%">Thread name</th>
                            <th width="40%">Thread Creator</th>
                            <th  width="14%">Last Poster</th>
                            <th width="15%">View Count</th>
                        </tr>  
                        <% //DISPLAY ALL THREADS MATCHING PASSED IN SUBCATEGORYID
                            myQuery = "SELECT * FROM threads INNER JOIN users ON threads.lastposter=users.userid WHERE subcategoryid='" + subcategoryid + "';";   
                            myQuery2 = "SELECT * FROM threads INNER JOIN users ON threads.threadcreator=users.userid WHERE subcategoryid='" + subcategoryid + "';";   
                            rs = statement.executeQuery(myQuery);
                            rs2 = statement2.executeQuery(myQuery2);
                                            
                            while(rs.next()){
                                rs2.next();
                                out.println("<tr><td><a href=\"post_structure.jsp?threadid=" 
                                        + rs.getString("threadid") + "&pageid=1\">" + rs.getString("threadname") + "</td>");
                                out.println("<td><a href=\"user.jsp/?userid=" + rs2.getInt("userid") + "\">" +  rs2.getString("username") + "</td>");
                                out.println("<td><a href=\"user.jsp/?userid=" + rs.getInt("userid") + "\">" + rs.getString("username") + "</td>");
                                out.println("<td>" + rs.getInt("viewcount") + "</td>");
                            }
                        %>		
                    </table>
                 
                    <div id="newthreadbutton">
                        </br>
                        <%  //WHEN CREATE NEW THREAD IS CLICKED, PASS ON TO newthread.jsp 
                            String temp = "newthread.jsp?threadid=" + threadid + "&subcategoryid=" + subcategoryid + "&categoryid=" + categoryid;
                            String url = "<form action=\"" + temp + "\"method=\"post\">";
                            out.println(url);
                        %>    
                            <input type="hidden" name="userid" value="<%=userid%>"/>
                            <input type="submit" value="Create new thread" />
                        </form>            
                    </div>                        
		</div>
            </div>		
	</body>
</html>

<%
    rs.close();
    rs2.close();
    connection.close();
%>