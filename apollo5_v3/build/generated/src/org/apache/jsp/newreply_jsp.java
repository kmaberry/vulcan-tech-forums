package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.io.*;
import javax.servlet.*;
import database.*;
import postsystem.*;

public final class newreply_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"newreply.css\">\n");
      out.write("\t\t<title>Vulcan Tech Forums</title>\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("\t\t<div id=\"NavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>\n");
      out.write("\t\t\t\t<li><a href=\"index.jsp.html\">Home</a></li>\n");
      out.write("\t\t\t\t<li class=\"active\"><a href=\"forums.jsp\">Forums</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"blogs.jsp\">Blogs</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"wiki.jsp\">Wiki</a></li>\n");
      out.write("\t\t\t\t<span id=\"signup\"><li><a href=\"signup.jsp\">Sign Up</a></li></span>\n");
      out.write("\t\t\t\t<span id=\"signin\"><li><a href=\"signin.jsp\">Sign In</a></li></span>\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("            </div>\n");
      out.write("\t\t<div id=\"addreplybody\">\n");
      out.write("                    \n");
      out.write("                    <h1>Reply Content:<h1/><br/>\n");
      out.write("\n");
      out.write("                        <form action=\"replyController\" name=\"replycontents\" method=\"post\">\n");
      out.write("                        <textarea id=\"replycontenttext\" class=\"text\" cols=\"86\" rows =\"20\" name=\"replycontenttext\"></textarea>\n");
      out.write("                        </br>\n");
      out.write("                        <input type=\"submit\" value=\"Reply\" class=\"submitButton\">\n");
      out.write("                        </form>\t\n");
      out.write("\n");
      out.write("                        \n");
      out.write("                        \n");
      out.write("\t\t</div>\t       \n");
      out.write("\t</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
