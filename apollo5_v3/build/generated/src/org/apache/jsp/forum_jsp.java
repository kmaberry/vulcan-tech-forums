package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import database.DB_Connect;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import javax.sql.rowset.CachedRowSet;

public final class forum_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"forum.css\">\n");
      out.write("\t\t<title>Vulcan Tech Forums</title>\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("\t\t<div id=\"NavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>\n");
      out.write("\t\t\t\t<li><a href=\"index.html\">Home</a></li>\n");
      out.write("\t\t\t\t<li class=\"active\"><a href=\"forums.html\">Forums</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"blogs.html\">Blogs</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"wiki.html\">Wiki</a></li>\n");
      out.write("\t\t\t\t<span id=\"signup\"><li><a href=\"signup.html\">Sign Up</a></li></span>\n");
      out.write("\t\t\t\t<span id=\"signin\"><li><a href=\"signin.html\">Sign In</a></li></span>\t\t\t\t\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div id=\"ForumNavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li>Welcome Anonymous</li>\n");
      out.write("\t\t\t\t<li>Notifications: <a href=\"index.html\">0</a></li>\n");
      out.write("\t\t\t\t<li>New Posts: <a href=\"forums.html\">0</a></li>\t\t\t\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div id=\"body\">\n");
      out.write("\t\t<div id=\"sectionHeader\">\n");
      out.write("                                        <table>\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<h2>General Discussion</h2>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<th width=\"34%\">Subsection name</th>\n");
      out.write("\t\t\t\t\t\t<th width=\"40%\">Recent thread </th>\n");
      out.write("\t\t\t\t\t\t<th  width=\"14%\">Post count</th>\n");
      out.write("\t\t\t\t\t\t<th width=\"15%\">Thread count</th>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("                        \n");
      out.write("\t\t\t\t\t");

                                        try{    
                                            String driver = "com.mysql.jdbc.Driver", username = "apollo5", password = "Vulcanforums";
                                            Class.forName(driver);

                                            String dbURL = "jdbc:mysql://localhost:3306/apollo5_vulcan";
                                            Connection mycon = DriverManager.getConnection(dbURL, username, password);

                                            Statement myStat = mycon.createStatement();
                                            Statement myStat2 = mycon.createStatement();
                                            String query = "SELECT * FROM threads";
                                            String query2 = "SELECT * FROM subcategory";
                                            
                                            ResultSet rs = myStat.executeQuery(query);
                                            ResultSet rs2 = myStat.executeQuery(query2);
                                            //ResultSet rs2 = myStat.executeQuery(query);
                                            int threadcount = 0;
                                            out.println("<tr>");
                                            while(rs.next()){
                                                out.println("<td>" + rs.getString("threadname") + "</td>");
                                                out.println("<td>" + rs2.getString("title") + "</td>");
                                                out.println("<td>" + rs.getInt("postcount") + "</td>");
                                                //threadcount++;
                                                out.println("<td>2</td>");
                                                rs2.next();
                                            }
                                            
                                            out.println("</tr>");
                                        }catch(SQLException err){
                                            err.printStackTrace();
                                        }
                                        
                                        
      out.write("\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<td><a href=\"text-edit.html\">Text Editors<a></td>\n");
      out.write("\t\t\t\t\t\t<td><a href=\"post_structure.html\">Is vim or emacs better</a></td>\n");
      out.write("\t\t\t\t\t\t<td>1,000,000</td>\n");
      out.write("\t\t\t\t\t\t<td>2</td>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t</table>\n");
      out.write("\n");
      out.write("                                        <div id=\"newthreadbutton\">\n");
      out.write("                                            </br>\n");
      out.write("                                            <form action=\"newthread.jsp\">\n");
      out.write("                                                <input type=\"submit\" value=\"Create new thread\" />\n");
      out.write("                                            </form>\n");
      out.write("                                            \n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("\t\t\t\t\t<table>\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<h2>Not so general discussion</h2>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<th width=\"34%\">Subsection name</th>\n");
      out.write("\t\t\t\t\t\t<th width=\"40%\">Recent thread </th>\n");
      out.write("\t\t\t\t\t\t<th  width=\"14%\">Post count</th>\n");
      out.write("\t\t\t\t\t\t<th width=\"15%\">Thread count</th>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t<td><a href=\"text-edit.html\">Google Pixel</a></td>\n");
      out.write("\t\t\t\t\t\t<td><a href = \"post_structure.html\">how do i turn my phone on? please help </a></td>\n");
      out.write("\t\t\t\t\t\t<td>506</td> \n");
      out.write("\t\t\t\t\t\t<td>50</td>\n");
      out.write("\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t</table>\n");
      out.write("                                        \n");
      out.write("                                        <div id=\"newthreadbutton\">\n");
      out.write("                                            </br>\n");
      out.write("                                            <form action=\"newthread.jsp\">\n");
      out.write("                                                <input type=\"submit\" value=\"Create new thread\" />\n");
      out.write("                                            </form>\n");
      out.write("                                            \n");
      out.write("                                        </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t</div>\t\t\n");
      out.write("\t</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
