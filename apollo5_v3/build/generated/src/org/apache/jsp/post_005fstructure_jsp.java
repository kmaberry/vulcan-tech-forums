package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class post_005fstructure_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/post_structure.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/forum.css\">\n");
      out.write("\t\t<title>Vulcan Tech Forums</title>\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("\t\t<div id=\"NavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>\n");
      out.write("\t\t\t\t<li class=\"active\"><a href=\"index.html\">Home</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"forums.html\">Forums</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"blogs.html\">Blogs</a></li>\n");
      out.write("\t\t\t\t<li><a href=\"wiki.html\">Wiki</a></li>\n");
      out.write("\t\t\t\t<span id=\"signup\"><li><a href=\"signup.html\">Sign Up</a></li></span>\n");
      out.write("\t\t\t\t<span id=\"signin\"><li><a href=\"signin.html\">Sign In</a></li></span>\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div id=\"ForumNavBar\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t\t<li>Welcome Anonymous</li>\n");
      out.write("\t\t\t\t<li>Notifications: <a href=\"index.html\">0</a></li>\n");
      out.write("\t\t\t\t<li>New Posts: <a href=\"forums.html\">0</a></li>\t\t\t\t\n");
      out.write("\t\t\t</ul>\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<br><br>\n");
      out.write("\t\t<div id=\"body\">\n");
      out.write("\n");
      out.write("\t\t<div id=\"post\">\n");
      out.write("\t\t<!--<h2>Is vim or emacs better</h2>99-->\n");
      out.write("\t\t\t<div id=\"userInfo\">\n");
      out.write("\t\t\t\t<img src=\"user.jpg\" alt=\"Someone\" height=\"60\" width=\"60\" align=\"center\">\n");
      out.write("\t\t\t\t<p><i><a href=\"#\">sramsey</a></i></p>\n");
      out.write("\t\t\t\t\t<div id=\"UInfo\">\n");
      out.write("\t\t\t\t\t\t<p>Posted: 10/8/16 at 4:52P.M.\n");
      out.write("\t\t\t\t\t</div>\t\n");
      out.write("\t\t\t</div>\t\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"communication\">\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Quote</button>\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Reply</button>\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"postelement\">\n");
      out.write("\t\t\t\t<p>Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do. Once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it, \"and what is the use of a book,\" thought Alice, \"without pictures or conversations?\" So she was considering in her own mind (as well as she could, for the day made her feel very sleepy and stupid), whether the pleasure of making a daisy-chain would be worth the trouble of getting up and picking the daisies, when suddenly a White Rabbit with pink eyes ran close by her. <br><br>Just a test for text wrapping</p>\n");
      out.write("\t\t\t\t<br>\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t<div id=\"post\">\n");
      out.write("\t\t<h2>Is vim or emacs better</h2>\n");
      out.write("\t\t\t<div id=\"userInfo\">\n");
      out.write("\t\t\t\t<img src=\"user.jpg\" alt=\"Someone\" height=\"60\" width=\"60\" align=\"center\">\n");
      out.write("\t\t\t\t<p><i><a href=\"#\">fresh_techie113</a></i></p>\n");
      out.write("\t\t\t\t\t<div id=\"UInfo\">\n");
      out.write("\t\t\t\t\t\t<p>Posted: 10/8/16 at 5:33P.M.\n");
      out.write("\t\t\t\t\t</div>\t\n");
      out.write("\t\t\t</div>\t\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"communication\">\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Quote</button>\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Reply</button>\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"postelement\">\n");
      out.write("\t\t\t\t<p>Scott likes Vim, so Vim :)</p>\n");
      out.write("\t\t\t\t<br>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t<div id=\"post\">\n");
      out.write("\t\t<h2>Is vim or emacs better</h2>\n");
      out.write("\t\t\t<div id=\"userInfo\">\n");
      out.write("\t\t\t\t<img src=\"user.jpg\" alt=\"Someone\" height=\"60\" width=\"60\" align=\"center\">\n");
      out.write("\t\t\t\t<p><i><a href=\"#\">schadde</a></i></p>\n");
      out.write("\t\t\t\t\t<div id=\"UInfo\">\n");
      out.write("\t\t\t\t\t\t<p>Posted: 10/8/16 at 11:59P.M.\n");
      out.write("\t\t\t\t\t</div>\t\n");
      out.write("\t\t\t</div>\t\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"communication\">\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Quote</button>\n");
      out.write("\t\t\t\t<button type=\"button\" onclick=\"window.location.href='newreply.html' \">Reply</button>\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"postelement\">\n");
      out.write("\t\t\t\t<p>No I dont! Emacs all the way!</p>\n");
      out.write("\t\t\t\t<br>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t</div>\t\t\n");
      out.write("\t</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
