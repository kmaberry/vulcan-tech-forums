<%-- 
    Document   : replysuccess
    Created on : Nov 30, 2016, 4:07:56 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%String e = request.getParameter("threadid");%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" type="text/css" href="main.css">
        <title>Confirmation</title>
    </head>
    <body>
        
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li ><a href="index.jsp">Home</a></li>
				<li class="active"><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\"><input type=\"hidden\" name=\"uname\" value=\"" + name + "\"><A HREF=\"javascript:document.userPage.submit()\">" + name + "</A></form></li>"); 
            out.println("<li><A HREF=\"signout.jsp\">Signout</A></li>");
        %>
        

        <%}  
        else{ 
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.jsp">Sign In</a></li></span>
        <% 
        }    
        %>
        </div>
		<div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
        
        </br> </br>
        <h1>Reply successfully processed!</h1>
    </body>
    <%
        //String url = "post_structure.jsp?threadid=" + request.getParameter("threadid") + "&pageid=1";
    
        //out.println(url);
        %>
        Redirect to correct page coming soon!
        <!--<form name="go" method="POST" action="/forums.jsp">
            <input type="hidden"> 
        </form>-->
    <script>
        setTimeout(function() {
        //window.location.href = url;
        //window.location.href = "threads.jsp";
        //out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\"><input type=\"hidden\" name=\"uname\" value=\"" + name + "\"><A HREF=\"javascript:document.userPage.submit()\">" + name + "</A></form></li>");
        //document.go.submit();
        //document.location = "threads.jsp?"; //I could not figure out how to get the page redirection to work right
        //If it would have worked right, it would have brought the user back to the page of which they clicked reply

        }, 4000);
    </script>
</html>
