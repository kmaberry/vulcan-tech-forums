<%-- 
    Document   : signout
    Created on : Dec 7, 2016, 9:19:04 PM
    Author     : Kasey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <% session.invalidate();
 response.sendRedirect("https://weave.cs.nmt.edu/apollo.5/vulcanforums/index.jsp");
 %>
 
</html>
