/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vulcan;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author SUN
 */
public class user extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet user</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet user at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {            
            String uname = request.getParameter("uname");            
            try {
            DB_Con DB_C = new DB_Con();
            Connection con = DB_C.DB_Con();
            Statement statement = con.createStatement();
            String myQuery = "SELECT username, profilepicture, postcount, datejoined, about, interests FROM users where username='"+uname+"';"; 
            ResultSet rs = statement.executeQuery(myQuery);   
            if (rs.next()==false){System.err.println("Query executation failure");}
            else{
            request.setAttribute("uname", rs.getString("username"));
            request.setAttribute("profilepic", rs.getString("profilepicture"));
            request.setAttribute("datejoined", rs.getString("datejoined"));
            request.setAttribute("postcount", rs.getString("postcount"));
            request.setAttribute("about", rs.getString("about"));
            request.setAttribute("interests", rs.getString("interests")); 
            if(request.getParameter("edit")!=null)
                request.getRequestDispatcher("editprofile.jsp").forward(request, response);                
            else 
                request.getRequestDispatcher("user.jsp").forward(request, response);            
            }
            rs.close();
            statement.close();
            con.close();
            } catch (Exception e) {
            System.err.println("Error with connection: " + e);
            }   
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
