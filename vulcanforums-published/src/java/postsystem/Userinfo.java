/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package postsystem;

/**
 *
 * @author Steven
 */
public class Userinfo {
    
    private int userid = 1;
    private String username = "admin";
    private String hashedpass = "temp";
    private String dynamicsalt = "temp";
    private String email = "temp@temp.temp"; 
    private int usergroup = 3;
    private int postcount = 5;
    private String datajoined = "2016-11-23 19:18:57"; //of type timestamp
    private String lastactivity = "2016-11-23 19:18:57"; //of type timestamp
    private int isbanned = 0;
    private String banduration = "0000-00-00 00:00:00"; //of type datetime
    private String ipaddress = "127.0.0.1";
    private String about = "administrator";
    private String intrest = "Banning people";
    private String profilepic = "http://i.imgur.com/4ybg2jx.jpg";

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedpass() {
        return hashedpass;
    }

    public void setHashedpass(String hashedpass) {
        this.hashedpass = hashedpass;
    }

    public String getDynamicsalt() {
        return dynamicsalt;
    }

    public void setDynamicsalt(String dynamicsalt) {
        this.dynamicsalt = dynamicsalt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUsergroup() {
        return usergroup;
    }

    public void setUsergroup(int usergroup) {
        this.usergroup = usergroup;
    }

    public int getPostcount() {
        return postcount;
    }

    public void setPostcount(int postcount) {
        this.postcount = postcount;
    }

    public String getDatajoined() {
        return datajoined;
    }

    public void setDatajoined(String datajoined) {
        this.datajoined = datajoined;
    }

    public String getLastactivity() {
        return lastactivity;
    }

    public void setLastactivity(String lastactivity) {
        this.lastactivity = lastactivity;
    }

    public int isIsbanned() {
        return isbanned;
    }

    public void setIsbanned(int isbanned) {
        this.isbanned = isbanned;
    }

    public String getDatatime() {
        return banduration;
    }

    public void setDatatime(String banduration) {
        this.banduration = banduration;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getIntrest() {
        return intrest;
    }

    public void setIntrest(String intrest) {
        this.intrest = intrest;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }
        
    
 
}
