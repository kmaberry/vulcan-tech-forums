-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 23, 2016 at 07:26 PM
-- Server version: 10.1.17-MariaDB-1~jessie
-- PHP Version: 5.6.24-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apollo5_vulcan`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`categoryid` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryid`, `name`, `description`) VALUES
(1, 'Main Section', 'test section put everything here');

-- --------------------------------------------------------

--
-- Table structure for table `moderators`
--

CREATE TABLE IF NOT EXISTS `moderators` (
  `userid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`postid` int(11) NOT NULL,
  `threadid` int(11) NOT NULL COMMENT 'foreign key',
  `posterid` int(11) NOT NULL COMMENT 'foreign key',
  `posttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `postcontents` text CHARACTER SET latin1 NOT NULL COMMENT 'same text limit as reddit'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`postid`, `threadid`, `posterid`, `posttime`, `postcontents`) VALUES
(1, 1, 1, '2016-11-24 02:22:19', 'spam'),
(2, 1, 1, '2016-11-24 02:22:30', 'spamm'),
(3, 1, 1, '2016-11-24 02:22:42', 'spammm'),
(4, 1, 1, '2016-11-24 02:22:56', 'spammmm'),
(5, 1, 1, '2016-11-24 02:23:08', 'spammmmm'),
(6, 1, 1, '2016-11-24 02:23:21', 'spammmmmm'),
(7, 1, 1, '2016-11-24 02:23:35', 'spammmmmmm'),
(8, 1, 1, '2016-11-24 02:23:53', 'spammmmmmmm'),
(9, 1, 1, '2016-11-24 02:24:07', 'spammmmmmmmm'),
(10, 1, 1, '2016-11-24 02:24:20', 'spammmmmmmmmm'),
(11, 1, 1, '2016-11-24 02:24:35', 'spammmmmmmmmmm');

-- --------------------------------------------------------

--
-- Table structure for table `private_messages`
--

CREATE TABLE IF NOT EXISTS `private_messages` (
`messageid` int(11) NOT NULL,
  `fromuserid` int(11) NOT NULL,
  `touserid` int(11) NOT NULL,
  `isread` tinyint(1) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `private_messages`
--

INSERT INTO `private_messages` (`messageid`, `fromuserid`, `touserid`, `isread`, `title`, `body`) VALUES
(1, 1, 1, 0, 'here is a message', 'have fun');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
`subcategoryid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`subcategoryid`, `categoryid`, `title`, `description`) VALUES
(1, 1, 'main subcategory', 'main subcategory for testing');

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
`threadid` int(11) NOT NULL,
  `threadname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lastposter` int(11) NOT NULL COMMENT 'foreign key',
  `postcount` int(11) NOT NULL,
  `threadcreator` int(11) NOT NULL,
  `viewcount` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL COMMENT 'foreign key',
  `subcategoryid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`threadid`, `threadname`, `lastposter`, `postcount`, `threadcreator`, `viewcount`, `categoryid`, `subcategoryid`) VALUES
(1, 'spam here', 1, 11111, 1, 1111, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`userid` int(11) NOT NULL,
  `username` varchar(16) CHARACTER SET latin1 NOT NULL,
  `hashedpass` varchar(255) CHARACTER SET latin1 NOT NULL,
  `dynamicsalt` varchar(16) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `usergroup` int(11) NOT NULL DEFAULT '1',
  `postcount` int(11) NOT NULL DEFAULT '0',
  `datejoined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastactivity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isbanned` tinyint(1) NOT NULL DEFAULT '0',
  `banduration` datetime NOT NULL,
  `ipaddress` varchar(15) CHARACTER SET latin1 NOT NULL,
  `about` text CHARACTER SET latin1 NOT NULL,
  `interests` text CHARACTER SET latin1 NOT NULL,
  `profilepicture` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `hashedpass`, `dynamicsalt`, `email`, `usergroup`, `postcount`, `datejoined`, `lastactivity`, `isbanned`, `banduration`, `ipaddress`, `about`, `interests`, `profilepicture`) VALUES
(1, 'admin', 'temp', 'temp', 'temp@temp.temp', 3, 5, '2016-11-24 02:18:57', '2016-11-24 02:18:57', 0, '0000-00-00 00:00:00', '127.0.0.1', 'administrator. Password will not work.', 'banning people', 'http://i.imgur.com/4ybg2jx.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`categoryid`);

--
-- Indexes for table `moderators`
--
ALTER TABLE `moderators`
 ADD KEY `userid` (`userid`), ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`postid`), ADD KEY `threadid` (`threadid`,`posterid`), ADD KEY `posterid` (`posterid`);

--
-- Indexes for table `private_messages`
--
ALTER TABLE `private_messages`
 ADD PRIMARY KEY (`messageid`), ADD KEY `fromuserid` (`fromuserid`,`touserid`), ADD KEY `touserid` (`touserid`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
 ADD PRIMARY KEY (`subcategoryid`), ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
 ADD PRIMARY KEY (`threadid`), ADD KEY `categoryid` (`categoryid`), ADD KEY `lastposter` (`lastposter`), ADD KEY `subcategoryid` (`subcategoryid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`userid`), ADD KEY `usergroup` (`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `categoryid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `postid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `private_messages`
--
ALTER TABLE `private_messages`
MODIFY `messageid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
MODIFY `subcategoryid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
MODIFY `threadid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `moderators`
--
ALTER TABLE `moderators`
ADD CONSTRAINT `moderators_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
ADD CONSTRAINT `moderators_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `category` (`categoryid`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`threadid`) REFERENCES `threads` (`threadid`),
ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`posterid`) REFERENCES `users` (`userid`);

--
-- Constraints for table `private_messages`
--
ALTER TABLE `private_messages`
ADD CONSTRAINT `private_messages_ibfk_1` FOREIGN KEY (`fromuserid`) REFERENCES `users` (`userid`),
ADD CONSTRAINT `private_messages_ibfk_2` FOREIGN KEY (`touserid`) REFERENCES `users` (`userid`);

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`categoryid`);

--
-- Constraints for table `threads`
--
ALTER TABLE `threads`
ADD CONSTRAINT `threads_ibfk_1` FOREIGN KEY (`lastposter`) REFERENCES `users` (`userid`),
ADD CONSTRAINT `threads_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `category` (`categoryid`),
ADD CONSTRAINT `threads_ibfk_3` FOREIGN KEY (`subcategoryid`) REFERENCES `subcategory` (`subcategoryid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
