<%-- 
    Document   : confirmacc
    Created on : Dec 3, 2016, 2:02:29 AM
    Author     : Kasey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Thank you for creating your account. Redirecting to home page in 3 seconds</h1>
        <script>
            window.setTimeout(function(){ window.location = "index.jsp";}, 3000);

        
        </script>
    </body>
</html>
