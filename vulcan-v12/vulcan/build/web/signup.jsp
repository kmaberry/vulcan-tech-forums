<%@page import="vulcan.DB_Con"%>
<%@page import="org.mindrot.jbcrypt.BCrypt"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
	<head>
		<title>Vulcan Tech Forums - Registration</title>
		<link rel="stylesheet" type="text/css" href="css/signup.css">
	</head>
	<body>
	<div id="reg">
        <form action="signup.jsp" method="post">
		<table align="center">
			<th colspan="2">Join the Vulcan Tech Forums</th>
			<tr>
				<td>Username:</td>
				<td><input required id="regusername" type="text" name="username" size="40" maxlength="15"/></td>
                        <script>
                            //use html5 validity api
                        
                        </script>
                        
            </tr>
			<tr></tr>
			<tr>
				<td>Password:</td>
				<td><input required id="regpwd" type="password" name="pwd" size="40" maxlength="20"/></td>
                        <script>
                            //error check data here
                        </script>
                                
            </tr>
			<tr></tr>
			<tr>
				<td>Confirm Password:</td>
				<td><input required id="regpwdconfirm" type="password" name="pwdconfirm" size="40" maxlength="20"/></td>
                        <script>
                            //error check data here
                        </script>
            </tr>
			<tr></tr>
            <tr>
				<td>Email Address:</td>
				<td><input required id="regemail" type="text" name="email" size="40" maxlength="40"/></td>
                        <script>
                            //error check data here
                        </script>
            </tr>
			<tr></tr>
			<tr>
				<td>Confirm Email Address:</td>
				<td><input required id="regemailconfirm" type="text" name="emailconfirm" size="40" maxlength="40"/></td>
                        <script>
                            //error check data here
                        </script>
            </tr>
			<tr></tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" id="button1" value="Submit"/>
					<input type="reset" id="button2" value="Reset"/>
				</td>
			</tr>
		</table>
        </form>
            <%
            String uname = request.getParameter("username");
            if(uname == null){
            //check if post went through or not    
            }else {
                String pwd = request.getParameter("pwd");
                String cpwd = request.getParameter("pwdconfirm");
                String email = request.getParameter("email");
                String cemail = request.getParameter("emailconfirm");
                try{
                    DB_Con DB_C = new DB_Con();
                    Connection connection = DB_C.DB_Con();
                    Statement statement = connection.createStatement();

                    String myQuery = "SELECT * FROM users WHERE username='" + uname + "';";
                    ResultSet rs = statement.executeQuery(myQuery);
                    if(rs.next()) {
                        //todo add a better way to do this
                        out.println("<script>alert(\"That username is already taken!\");</script>");
                    } else {
                        String pw_hash = BCrypt.hashpw(pwd, BCrypt.gensalt(10));
                        myQuery = "INSERT INTO users (username, hashedpass, dynamicsalt, email, ipaddress) VALUES ('"+uname+"','"+pw_hash+"', 'aaaaaaaa', '"+email+"', '127.0.0.1')";

                        int ri = statement.executeUpdate(myQuery);
                        if(ri != 0){
                            out.println("<script>alert(\"Account created!\");</script>");
                            String redirectURL = "https://weave.cs.nmt.edu/apollo.5/vulcan/confirmacc.jsp";
                            response.sendRedirect(redirectURL);
                        } else {
                            //shouldn't happen but maybe database entry fails
                            out.println("<script>alert(\"Account creation failed!\");</script>");
                        }
                    }
                    
                    rs.close();
                    connection.close();
                } catch (Exception ex){
                    System.err.println("Error with connection");
                }
            }
            //todo: hash passwords
            %>
	</div>
	</body>
</html>