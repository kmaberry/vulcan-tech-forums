<%-- 
    Document   : viewprofile
    Created on : Dec 5, 2016, 6:25:14 PM
    Author     : SUN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
	<head>
                <link rel="stylesheet" type="text/css" href="post_structure.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        
        <span style="float:right; height: 10px;"> 
            <% out.println("<li class=\"active\"><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        </span>

        <%}  
        else{ 
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
                        </ul>
        </div>	
        <div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
                <br><br>
		
                <div id="body">

		<div id="post">
                    <h2><%=request.getAttribute("uname")%></h2>
			<div id="userInfo">
				<img src="<%=request.getAttribute("profilepic")%>" alt="Someone" height="60" width="60" align="center">
				<p><i><%=request.getAttribute("uname")%></i></p>
					<div id="UInfo">
						<p>Post Count: <%=request.getAttribute("postcount")%> </p>
						<p>Date joined: <%=request.getAttribute("datejoined")%></p>
						<p>Profile Views: 10</p>
					</div>	
			</div>	
				<h3>About Me</h3>
				<p><%=request.getAttribute("about")%></p>
				<h3>Interests</h3>
				<p><%=request.getAttribute("interests")%></p>
				<br>                                 
                                <% if (request.getAttribute("uname").equals(name)){%>
                                <%--<form action="${pageContext.request.contextPath}/user" method="post">
                                <input type="hidden" name="uname" value=<%=name%>>
                                <input type="hidden" name="edit" value=1>
                                <span style="float:right">
                                <p><input type="submit" value="Edit Profile"></p>
                                </span>
                                </form>--%>
                                <span style="float:right;"> 
                    <% out.println("<form name=\"userPages\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<input type=\"hidden\" name=\"edit\" value=\"" + 1 + "\">"
                    + "<A HREF=\"javascript:document.userPages.submit()\">" + "Edit Profile" + "</A>"
                    + "</form>");
                    %>            
                                </span>
                                <%}%>                     
                                                           
		</div>
		
		</div>		
	</body>
</html>
