<%-- 
    Document   : manage
    Created on : Dec 5, 2016, 8:16:47 PM
    Author     : SUN
--%>

<%@page import="vulcan.DB_Con"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<% 
    DB_Con DB_C = new DB_Con();
    Connection connection = DB_C.DB_Con();
    Statement statement = connection.createStatement();
    

    String myQuery;
    ResultSet rs = null;
    String cat = null;
    %>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/forum_manage.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li class="active"><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> 
            <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        

        <%}  
        else{
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
        </div>	
		<div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
		
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Management</h2>
				<ul>
				<li class="active"><a href="manage.jsp">Category Lists</a></li>
				<%--<li><a href="newcategory.jsp">Add New Category</a></li>--%>				
				<li><a href="usermanage.jsp">User Management</a></li>
                    
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
					
					<%
                                            cat = request.getParameter("catid");
                                            if (cat != null){
                                                ArrayList<String> tmp = new ArrayList<String>();
                                                String catname;
                                                int i = 0;
                                                myQuery = "SELECT name FROM category WHERE categoryid=\""+ cat + "\";";
                                                rs = statement.executeQuery(myQuery);
                                                rs.next();
                                                out.println("<table><tr><h2>"+ rs.getString("name")+"</h2></tr>");
                                                out.println("<tr><th width=\"34%\">Subsection name</th><th width=\"40%\">Recent thread </th><th  width=\"14%\">Post count</th><th width=\"15%\">Thread count</th></tr>");
                                                        
                                                myQuery = "SELECT subcategory.subcategoryid FROM subcategory WHERE subcategory.categoryid=\""+ cat + "\";";
                                                rs = statement.executeQuery(myQuery);
                                                while(rs.next()){
                                                    tmp.add(rs.getString("subcategoryid"));
                                                    //out.println(rs.getString("subcategoryid"));
                                                }
                                                for (String b : tmp) {
                                                //myQuery = "SELECT subcategory.title, category.name, subcategory.subcategoryid, threads.threadid, threads.threadname FROM subcategory ORDER BY ;";
                                                    myQuery = "SELECT subcategory.title, category.name, subcategory.subcategoryid, threads.threadid, threads.threadname, users.username FROM subcategory "
                                                            + "JOIN (SELECT threads.threadid, threads.lastposter, threads.subcategoryid FROM threads WHERE threads.subcategoryid='"+ b +"' ORDER BY threads.lastpostdate DESC LIMIT 1) tmp "
                                                            + "JOIN threads ON threads.threadid = tmp.threadid "
                                                            + "JOIN users ON users.userid = tmp.lastposter "
                                                            + "JOIN category ON category.categoryid = subcategory.categoryid "
                                                            + "WHERE subcategory.subcategoryid = " + b +";";
                                                    //myQuery = "SELECT threadid FROM threads WHERE threads.subcategoryid='"+ b +"' ORDER BY threads.lastpostdate DESC LIMIT 1;";
                                                    rs = statement.executeQuery(myQuery);
                                                    while(rs.next()){
                                                    out.println("<tr>");
                                                    out.println("<td><form name=\"threads" + b + rs.getString("subcategoryid") +"\" method=\"POST\" action=\"threads.jsp\"><input type=\"hidden\" name=\"subcategoryid\" value=\"" + rs.getString("subcategoryid") +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.threads"  + b + rs.getString("subcategoryid") +".submit()\">" + rs.getString("subcategory.title") +"</A></form></td>");
                                                    out.println("<td><form name=\"posts" + b + rs.getString("threadid") +"\" method=\"POST\" action=\"post_structure.jsp\"><input type=\"hidden\" name=\"threadid\" value=\"" + rs.getString("threadid") +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.posts"  + b + rs.getString("threadid") +".submit()\">" + rs.getString("threadname") +"</A></form></td>");
                                                    out.println("<td>null</td>");
                                                    out.println("<td>null</td>");
                                                    out.println("</tr>");}
                                                    
                                                }
                                                out.println("</table>");
                                            } else {
                                                ArrayList<String> tmp = new ArrayList<String>();
                                                ArrayList<String> tmp2 = new ArrayList<String>();
                                                //ArrayList<String> tmp3 = new ArrayList<String>();
                                                String[] tmp3= null;
                                                String catname;
                                                int i = 0;
                                                myQuery = "SELECT DISTINCT categoryid FROM category;";
                                                rs = statement.executeQuery(myQuery);
                                                while(rs.next()) {
                                                    tmp2.add(rs.getString("categoryid"));
                                                    
                                                }
                                                for (String a: tmp2){ 
                                                    cat = a;
                                                    myQuery = "SELECT name FROM category WHERE categoryid=\""+ cat + "\";";
                                                    rs = statement.executeQuery(myQuery);
                                                    rs.next();
                                                    out.println("<table><tr><h2>"+ rs.getString("name")+"</h2></tr>");
                                                    out.println("<tr><th width=\"34%\">Subsection name</th><th width=\"40%\">Recent thread </th><th  width=\"14%\">Post count</th><th width=\"15%\">Thread count</th></tr>");

                                                    myQuery = "SELECT subcategory.subcategoryid FROM subcategory WHERE subcategory.categoryid=\""+ cat + "\";";
                                                    rs = statement.executeQuery(myQuery);
                                                    tmp.clear();
                                                    while(rs.next()){
                                                        tmp.add(rs.getString("subcategoryid"));
                                                        
                                                    }
                                                    for (String b : tmp) {
                                                        
                                                    //myQuery = "SELECT subcategory.title, category.name, subcategory.subcategoryid, threads.threadid, threads.threadname FROM subcategory ORDER BY ;";
                                                        myQuery = "SELECT subcategory.title, category.name, subcategory.subcategoryid, threads.threadid, threads.threadname, users.username FROM subcategory "
                                                                + "JOIN (SELECT threads.threadid, threads.lastposter, threads.subcategoryid FROM threads WHERE threads.subcategoryid='"+ b +"' ORDER BY threads.lastpostdate DESC LIMIT 1) tmp "
                                                                + "JOIN threads ON threads.threadid = tmp.threadid "
                                                                + "JOIN users ON users.userid = tmp.lastposter "
                                                                + "JOIN category ON category.categoryid = subcategory.categoryid "
                                                                + "WHERE subcategory.subcategoryid = " + b +";";
                                                        //myQuery = "SELECT threadid FROM threads WHERE threads.subcategoryid='"+ b +"' ORDER BY threads.lastpostdate DESC LIMIT 1;";
                                                        rs = statement.executeQuery(myQuery);
                                                        while(rs.next()){
                                                           
                                                        out.println("<tr>");
                                                        out.println("<td><form name=\"threads" + b + rs.getString("subcategoryid") +"\" method=\"POST\" action=\"threads.jsp\"><input type=\"hidden\" name=\"subcategoryid\" value=\"" + rs.getString("subcategoryid") +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.threads"  + b + rs.getString("subcategoryid") +".submit()\">" + rs.getString("subcategory.title") +"</A></form></td>");
                                                        out.println("<td><form name=\"posts" + b + rs.getString("threadid") +"\" method=\"POST\" action=\"post_structure.jsp\"><input type=\"hidden\" name=\"threadid\" value=\"" + rs.getString("threadid") +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.posts"  + b + rs.getString("threadid") +".submit()\">" + rs.getString("threadname") +"</A></form></td>");
                                                        out.println("<td>null</td>");
                                                        out.println("<td>null</td>");
                                                        out.println("</tr>");}

                                                    }
                                                    out.println("</table>");
                                                }
                                                
                                            }
                                            %>
                <form action="newcategory.jsp" method="post">
                    <input type="submit" value="Add a new Category" class="submitButton">   	 
                </form>
                <form action="newsubcategory.jsp" method="post">
                    <input type="submit" value="Add a new Subcategory" class="submitButton">   	 
                </form>
		</div>
				</div>
			</div>					
		</div>
			
	</body>
</html>
