package forumsystem.post;


import java.sql.*;
import java.util.*;
import database.*;
import java.text.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class populates the list to use for pagination of the post per thread
 * @author Steven
 */
public class postpage {
    
    public static List<Postfetch> getPost(int start, int perpage, String threadid) throws ParseException{
        
        try{
            
            List<Postfetch> postlist = new ArrayList<>();
            
            DB_Connect DB = new DB_Connect();
            Connection connection = DB.establishConnection();
            Statement statement = connection.createStatement();
            
            String query = "SELECT * FROM posts INNER JOIN users ON posts.posterid=users.userid WHERE threadid=" + threadid + " LIMIT " + (start-1) + "," + perpage;
            
            ResultSet rs = statement.executeQuery(query);
            
            DateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat outputformat = new SimpleDateFormat("MM-dd-yyyy KK:mm a");
            String tempdate = null;
            
            
            while(rs.next()){
                Postfetch post = new Postfetch();
                post.setPosterid(rs.getInt("userid"));
                post.setThreadid(rs.getInt("threadid"));
                post.setPostcontents(rs.getString("postcontents"));
                post.setPostid(rs.getInt("postid"));
                tempdate = outputformat.format(inputformat.parse(rs.getString("posttime")));
                post.settimeStamp(tempdate);
                //post.settimeStamp(rs.getString("posttime"));
                post.setProfilePicture(rs.getString("profilepicture"));
                post.setUsername(rs.getString("username"));
                postlist.add(post);
            }
            return postlist;
            
        }catch(SQLException err){
            err.printStackTrace();
        }  
        return null;
    }
}
