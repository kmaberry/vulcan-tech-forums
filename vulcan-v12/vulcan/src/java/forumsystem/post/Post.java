/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.post;

import database.DB_Connect;
import java.io.*;
import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Steven
 */
public class Post {
    
    private int threadid;
    private int posterid;
    private String postcontents;
    private String pagenum;
    private int postid;
 
    public int getThreadid() {
        return threadid;
    }

    public void setThreadid(int threadid) {
        this.threadid = threadid;
    }
    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public int getPosterid() {
        return posterid;
    }

    public void setPosterid(int posterid) {
        this.posterid = posterid;
    }

    public String getPostcontents() {
        return postcontents;
    }

    public void setPostcontents(String postcontents) {
        this.postcontents = postcontents;
    }
    
    public void setpagenum (String pagenum){
        this.pagenum = pagenum;
    }
  
    /*Used for newreply, it simply inserts a post into the designated threadid*/
    public void insertPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
            //PrintWriter out = response.getWriter();
        
            try{
                
                DB_Connect DB = new DB_Connect();
                Connection DB_connection = DB.establishConnection();
                
                String query = "INSERT INTO posts (threadid, posterid, postcontents) " + "VALUES ('"+this.threadid+"', '"+this.posterid+"', '"+this.postcontents+"')";

                Statement myStatement = DB_connection.createStatement();  

                try{
                    int executeUpdate = myStatement.executeUpdate(query);
                    myStatement.close();
                    DB_connection.close();
                    response.sendRedirect("https://weave.cs.nmt.edu/apollo.5/vulcanforums/forums.jsp");
                    //out.flush();
                }catch(SQLException err){
                    err.printStackTrace();
                   // out.println("SOME ERROR OCCURED: " + err);
                }
            }catch(SQLException | NullPointerException err){
                err.printStackTrace();     
                //out.println("SOME ERROR OCCURED: " + err);
            }
    }
    
    /*Used to reteieve post made by user*/
    public void retrievePost(HttpServletRequest request, HttpServletResponse response){
        
        try{
            DB_Connect DB = new DB_Connect();
            Connection DB_connection = DB.establishConnection();
            
            PrintWriter out = response.getWriter();
            
            String query = "SELECT * FROM posts WHERE posterid = " + this.posterid;
            
            Statement myStatement = DB_connection.createStatement();
            
            ResultSet rs = myStatement.executeQuery(query);
            
            while(rs.next()){
                out.printf("User ID = " + this.posterid);
                out.printf(" --> " + rs.getString("postcontents") + "</br>");
            }   
            
            out.flush();
            rs.close();
            DB_connection.close();
            
        }catch(SQLException | IOException err){
            err.printStackTrace();
        }
    }

    /*Used to insert initial post when creating a thread*/
    public int insertThreadPost() {
        try{
            DB_Connect DB = new DB_Connect();
            Connection DB_connection = DB.establishConnection();
            Statement myStatement = DB_connection.createStatement();
            Statement myStatement2 = DB_connection.createStatement();
            ResultSet rs = myStatement.executeQuery("SELECT MAX(threadid) AS threadid FROM threads;");
            rs.next();
            int max = rs.getInt("threadid");
            
            String query = "INSERT INTO posts (threadid, posterid, postcontents) " + "VALUES ('"+max+"', '"+posterid+"', '"+postcontents+"')";

            try{
                int executeUpdate = myStatement2.executeUpdate(query);
                myStatement.close();
                myStatement2.close();
                DB_connection.close();
                
                return max;
                    
            }catch(SQLException err){
                err.printStackTrace();
            }
                
        }catch(SQLException | NullPointerException err){
            err.printStackTrace();
        }
        return 1;
    }
}
