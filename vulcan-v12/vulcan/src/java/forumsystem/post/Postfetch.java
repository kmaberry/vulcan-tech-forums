/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forumsystem.post;

/**
 *
 * @author Steven
 */
public class Postfetch {
    
    private int threadid;
    private int posterid;
    private int postid;
    private String postcontents;
    private String timestamp;
    private String profilepicture;
    private String username;
 
    public int getThreadid() {
        return threadid;
    }

    public void setThreadid(int threadid) {
        this.threadid = threadid;
    }
    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public int getPosterid() {
        return posterid;
    }

    public void setPosterid(int posterid) {
        this.posterid = posterid;
    }
    
    public void settimeStamp(String timestamp){
        this.timestamp = timestamp;
    }
    
    public String gettimeStamp(){
        return timestamp;
    }

    public String getPostcontents() {
        return postcontents;
    }
       
    public void setPostcontents(String postcontents) {
        this.postcontents = postcontents;
    }
    
    public void setProfilePicture(String ProfilePicture){
        this.profilepicture = ProfilePicture;
    }
    
    public String getProfilePicture(){
        return profilepicture;
    }
    
    public void setUsername(String username){
        this.username = username;
    }
    
    public String getUsername(){
        return username;
    }
    
}