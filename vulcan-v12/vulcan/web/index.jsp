
<%@page import="vulcan.DB_Con"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<% 
    DB_Con DB_C = new DB_Con();
    Connection connection = DB_C.DB_Con();
    Statement statement = connection.createStatement();
    String myQuery;
    ResultSet rs;

    //just give it tmp data for now
    //TODO get rid of this and use one join
    String[] b = {"1","2","3","4","5","6","7","8","9","10"};
    String[] u = {"1","2","3","4","5","6","7","8","9","10"};
    %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
	<body>
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li class="active"><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> 
            <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        </span>

        <%}  
        else{ 
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
                        </ul>
        </div>	
        <div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Category</h2>
				<ul>
                                <%
                                    myQuery = "SELECT * FROM category";
                                    rs = statement.executeQuery(myQuery);
                                    while(rs.next()){
                                        if(!rs.getString("categoryid").equals("13")){
                                            out.println("<li><form name=\"category" +rs.getString("categoryid") +"\" method=\"POST\" action=\"forums.jsp\"><input type=\"hidden\" name=\"catid\" value=\"" +rs.getString("categoryid") +"\"><A HREF=\"javascript:document.category" +rs.getString("categoryid") +".submit()\">" +rs.getString("name") +"</A></form></li>");
                                        }
                                        //out.println("<li><a href=\"forums.jsp" +rs.getString("categoryid") +"\">" +rs.getString("name") +"</a></li>");
                                    }
                                %>
                                </ul>
			</div>
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
                                    
					<h2>News content</h2>
                                        <% myQuery = "SELECT threads.threadname, threads.threadcreator, threads.lastpostdate, threads.isNews, users.userid, threads.threadid, posts.postcontents, users.username FROM threads JOIN posts ON posts.threadid=threads.threadid JOIN users ON users.userid=threads.threadcreator WHERE threads.isNews='1' GROUP BY threads.threadid LIMIT 10;";
                                        rs = statement.executeQuery(myQuery);
                                        int z = 11;
                                        while(rs.next()){
                                            out.println("<h2>");
                                            out.println("<p><form name=\"posts" + z +"\" method=\"POST\" action=\"post_structure.jsp\"><input type=\"hidden\" name=\"threadid\" value=\"" + rs.getString("threads.threadid") +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.posts" + z +".submit()\">" + rs.getString("threads.threadname") +"</A></form>");
                                            //out.println(rs.getString("threads.threadname"));
                                            out.println("</h2><p>");
                                            z++;
                                            out.println(rs.getString("posts.postcontents")+ "<br>");
                                            out.println("<br> Reported by: ");
                                            out.println("<form name=\"userPage" + z +"\" method=\"POST\" action=\"user\"><input type=\"hidden\" name=\"uname\" value=\"" + rs.getString("users.username") + "\"><A HREF=\"javascript:document.userPage"+z+".submit()\">" + rs.getString("users.username") + "</A></form>");
                                            z++;
                                            out.println("</p>");
                                            
                                        }
                                    %>
                                    <h2>Todo</h2>
					<p>:
                                        <ul>
                                            <li>Ability to change profile picture...Done!</li>
                                            <li>New subcategories...Done!</li>
                                            <li>News Content..Done!</li>
                                            <li>Moderation capabilites..kind of. No report post feature or private messages. No subcategory moderators</li>
                                            <li>Forgot password reset</li>
                                            <li>Private Messaging</li>
                                            
                                            <li>Search</li>
                                        </ul></p>
                                        
				</div>
			</div>
			<div id="content-aside">
				<div class="inner">
					<h2>Latest Posts</h2> 
                                        <%
                                            myQuery = "SELECT * FROM posts ORDER BY posttime DESC LIMIT 10;";
                                            
                                            myQuery = "SELECT * FROM posts ORDER BY posttime DESC LIMIT 10;";
                                            rs = statement.executeQuery(myQuery);
                                            for(int i = 0; i < 10; i++){
                                                rs.next();
                                                b[i] = rs.getString("threadid");
                                                u[i] = rs.getString("posterid");
                                            }
                                            for(int i = 0; i < 10; i++){
                                                
                                                myQuery = "SELECT threads.threadname, users.userid, users.username FROM threads INNER JOIN users ON users.userid='" + u[i] +"';";
                                                rs = statement.executeQuery(myQuery);
                                                rs.next();
                                                //out.println("<p><a href=\"thread.jsp?c=" + b[i] +"\">" +rs.getString("threadname") +"</a> Posted by: <a href=\"user.jsp?c=" + u[i] +"\">" +rs.getString("username") +"</a></p><br>");
                                                out.println("<p><form name=\"posts" + i +"\" method=\"POST\" action=\"post_structure.jsp\"><input type=\"hidden\" name=\"threadid\" value=\"" +b[i] +"\"><input type=\"hidden\" name=\"pageid\" value=\"1\"><A HREF=\"javascript:document.posts" + i +".submit()\">" + rs.getString("threadname") +"</A></form> Posted By:");
                                                out.println("<form name=\"userPage" + i +"\" method=\"POST\" action=\"user\"><input type=\"hidden\" name=\"uname\" value=\"" + rs.getString("username") + "\"><A HREF=\"javascript:document.userPage"+i+".submit()\">" + rs.getString("username") + "</A></form>");
                                                out.println("</p><br>");
                                                
                                                //<li><form name=\"nextPage" +rs.getString("categoryid") +"\" method=\"POST\" action=\"forums.jsp\"><input type=\"hidden\" name=\"catid\" value=\"" +rs.getString("categoryid") +"\"><A HREF=\"javascript:document.nextPage" +rs.getString("categoryid") +".submit()\">" +rs.getString("name") +"</A></form></li>"
                                            }
                                        %>
				</div>
			</div>			
		</div>
		</div>		
	</body>
</html>
<%
    rs.close();
    connection.close();
%>