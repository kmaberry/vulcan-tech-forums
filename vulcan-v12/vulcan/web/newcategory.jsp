<%-- 
    Document   : newreply
    Created on : Nov 28, 2016, 6:37:25 PM
    Author     : Steven
--%>

<%@page import="vulcan.DB_Con"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.servlet.*"%>
<%@page import="database.*"%>
<%@page import="postsystem.*"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="newreply.css">
        <link rel="stylesheet" type="text/css" href="css/forum.css">
        <title>Vulcan Tech Forums</title>
    </head>
    <body>
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li class="active"><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> 
            <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        

        <%}  
        else{
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
        </div>	
		<div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
                                
            

		<div id="addreplybody">
                    
                        
                        
                        <form action="newcategory.jsp" name="threadnametext" method="post">
                            <h1>New Category</h1>
                            <p><font size="3">Category Title</font></p>
                            <textarea required id="categorytitle" class="text" cols="86" rows ="2" name="categorytitletext"></textarea>
                            
                        
                            <p><font size="3">Description</font></p>
                        
                            <textarea required id="threadposttext" class="text" cols="86" rows ="10" name="categorydesctext"></textarea>
                            <h1>New Subcategory</h1>
                            <p><font size="3">Subcategory Title</font></p>
                            <textarea required id="subcategorytitle" class="text" cols="86" rows ="2" name="subcategorytitletext"></textarea>
                            
                        
                            <p><font size="3">Description</font></p>
                        
                            <textarea required id="threadposttext" class="text" cols="86" rows ="10" name="subcategorydesctext"></textarea>
                            <h1>Starting Thread</h1>
                            <p><font size="3">Thread Title</font></p>
                            <textarea required id="subcategorytitle" class="text" cols="86" rows ="2" name="threadtitletext"></textarea>
                            
                        
                            <p><font size="3">Body</font></p>
                        
                            <textarea required id="threadposttext" class="text" cols="86" rows ="10" name="threadbodytext"></textarea>
                            <br><input type="submit" id="button1" value="Submit"/>
                        </form>	 
		</div>	   
                                <%
                
                String title = request.getParameter("categorytitletext");
                String desc = request.getParameter("categorydesctext");
                String subtitle = request.getParameter("subcategorytitletext");
                String subdesc = request.getParameter("subcategorydesctext");
                String threadtitle = request.getParameter("threadtitletext");
                String threadbody = request.getParameter("threadbodytext");
                //String desc = "AAAA";
                if(title == null){
                //check if post went through or not    
                } else {
                    DB_Con DB_C = new DB_Con();
                    Connection connection = DB_C.DB_Con();
                    Statement statement = connection.createStatement();
                    String myQuery;
                    ResultSet rs;
                    int posterid;
                    if(session.getAttribute("userid") != null){
                        posterid = Integer.parseInt((String)session.getAttribute("userid"));
                    
                        myQuery = "INSERT INTO category (name, description) VALUES ('"+title+"','"+desc+"')";

                        int ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"New Category Creation Failed!\");</script>");
                        }
                        /*myQuery = "UPDATE users SET users.usergroup=2 WHERE users.userid ="+posterid+";";
                        ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"Set User Role Failed!\");</script>");
                        }*/
                        myQuery = "SELECT categoryid FROM category WHERE name='"+title+"';";
                        rs = statement.executeQuery(myQuery);
                        rs.next();
                        String catid = rs.getString("categoryid");
                        //worry about this later
                        /*myQuery = "INSERT INTO moderators ;";
                        ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"Set User Role Failed!\");</script>");
                        }*/
                        myQuery = "INSERT INTO subcategory (categoryid, title, description) VALUES ('"+catid+"', '"+subtitle+"', '"+subdesc+"');";
                        ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"Add category failed!\");</script>");
                        }
                        myQuery = "SELECT subcategoryid FROM subcategory WHERE categoryid='"+catid+"';";
                        rs = statement.executeQuery(myQuery);
                        rs.next();
                        String subcatid = rs.getString("subcategoryid");
                        myQuery = "INSERT INTO threads (categoryid, lastposter, subcategoryid, threadcreator, threadname, postcount, viewcount) VALUES ('"+catid+"', "+posterid+", '"+subcatid+"', "+posterid+", '"+threadtitle+"', 1, 1);";
                        ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"Add default thread failed!\");</script>");
                        }
                        myQuery = "SELECT threadid FROM threads WHERE categoryid='"+catid+"';";
                        rs = statement.executeQuery(myQuery);
                        rs.next();
                        String threadid = rs.getString("threadid");
                        myQuery = "INSERT INTO posts (threadid, posterid, postcontents) VALUES ('"+threadid+"', "+posterid+", '"+threadbody+"');";
                        ri = statement.executeUpdate(myQuery);
                        if(ri == 0){
                            out.println("<script>alert(\"Add default thread failed!\");</script>");
                        }
                        out.println("<script>alert(\"Category and default subcategory succesfully added!\");</script>");
                    } else {
                        out.println("<script>alert(\"Only registered users can make a category!\");</script>");
                    }
                }
            %>
	</body>
</html>
