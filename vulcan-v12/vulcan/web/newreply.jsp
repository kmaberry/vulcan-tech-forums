<%-- 
    Document   : newreply
    Created on : Nov 28, 2016, 6:37:25 PM
    Author     : Steven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.servlet.*"%>
<%@page import="database.*"%>
<%@page import="postsystem.*"%>

<!DOCTYPE html>
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" type="text/css" href="main.css">
            <link rel="stylesheet" type="text/css" href="newreply.css">
            <link rel="stylesheet" type="text/css" href="forum.css">
            <title>Vulcan Tech Forums</title>
	</head>
        <body>
        <div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li><a href="index.jsp">Home</a></li>
				<li class="active"><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> 
            <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        

        <%}  
        else{
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
        </div>	
		<div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
            <script>
                function checkform() { //KEEP USER FROM SUBMITTING BLANK REPLIES
                    if(document.getElementById("replycontenttext").value === '') {
                        alert ('Reply content required');
                        return false;
                    } else {
                        document.replycontents.submit(); 
                    }
                }
            </script>
            
		<div id="addreplybody"> 
                    <%
                        //To pass the user to the servlet without showing in url
                        String userid = (String)session.getAttribute("username");
                        String pageident = request.getParameter("pageide");
                        String thethreadid = request.getParameter("threadid");
                    %>
                    <h1>Reply Content:</h1><br/>
                    <form onsubmit="return checkform()" action="replyController" name="replycontents" method="post">
                        <textarea id="replycontenttext" class="text" cols="90" rows ="17" name="replycontenttext"></textarea></br>
                        <input type="hidden" name="posterid" value="<%=userid%>">
                        <input type="hidden" name="threadid" value="<%=thethreadid%>">
                        <input type="hidden" name="pageide" value="<%=pageident%>">
                        <input type="submit" value="Submit" class="submitButton" onclick="checktext()">
                    </form>	     
            	</div>	       
	</body>
</html>