<%-- 
    Document   : usermanage
    Created on : Dec 5, 2016, 8:04:03 PM
    Author     : SUN
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="vulcan.DB_Con"%>
<%@page import="java.util.*"%>
<%@page import="vulcan.userattr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/forum_manage.css">
		<link rel="stylesheet" type="text/css" href="css/forum.css">
		<title>Vulcan Tech Forums</title>
	</head>
        <%  
            ArrayList<userattr> ulist = new ArrayList<userattr>();
            try {
            DB_Con DB_C = new DB_Con();
            Connection con = DB_C.DB_Con();
            Statement statement = con.createStatement();
            String myQuery = "SELECT username, email, isbanned, usergroup FROM users"; 
            ResultSet rs = statement.executeQuery(myQuery);         
            while(rs.next()){
                userattr u = new userattr();
                u.setName(rs.getString("username"));
                u.setEmail(rs.getString("email"));
                u.setGroupid(Integer.parseInt(rs.getString("usergroup")));
                u.setStatus(Integer.parseInt(rs.getString("isbanned")));
                ulist.add(u);
            }                           
            rs.close();
            statement.close();
            con.close();
            } catch (Exception e) {
            System.err.println("Error with connection: " + e);
            }
            %>
	<body>
		<div id="NavBar">
			<ul>
				<li><strong>Vulcan</strong><span>&nbsp;Tech Forums</span></li>
				<li ><a href="index.jsp">Home</a></li>
				<li><a href="forums.jsp">Forums</a></li>
				
        <% 
            String name=(String)session.getAttribute("username"); 
            String usergroup = (String)session.getAttribute("usergroup");
            if(name!=null){
                if(usergroup.equals("3")){
        %>
        <li class="active"><a href="manage.jsp">Administrator</a></li>
        <%}%>
        <span style="float:right;"> 
            <% out.println("<li><form name=\"userPage\" method=\"POST\" action=\"user\">"
                    + "<input type=\"hidden\" name=\"uname\" value=\"" + name + "\">"
                    + "<A HREF=\"javascript:document.userPage.submit()\">" + name + "</A>"
                    + "</form></li>");
             %>
            <li><A HREF="signout.jsp">Signout</A></li>
        

        <%}  
        else{
        name = "Anonymous";
        %>
        <span id="signup"><li><a href="signup.jsp">Sign Up</a></li></span>
        <span id="signin"><li><a href="signin.html">Sign In</a></li></span>
        <% 
        }    
        %>
        </div>	
		<div id="ForumNavBar">
			<ul>
				<li>Welcome <%=name%></li>
				<li>Notifications: <a href="index.jsp">0</a></li>
				<!--<li>New Posts: <a href="forums.jsp">0</a></li>	-->			
			</ul>	
		</div>
		
		<div id="body">
		<div id="aside">
			<div class="inner">
				<h2>Management</h2>
				<ul>
				<li><a href="manage.jsp">Category Lists</a></li>
				<%--<li><a href="newcategory.jsp">Add New Category</a></li>--%>				
                                <li><a href="usermanage.jsp">User Management</a></li>
				</ul>
			</div>                                
		</div>
		<div id="content">
			<div id="main">
				<div class="inner">
                                     
                                    <table width="900px;">
					<tr>
						<h2>User Management</h2>
					</tr>
					<tr>
						<th width="20%">user name</th>
						<th width="30%">user email</th>						
						<th width="20%">user roles </th>
                                                <th width="15%">user status </th>
						<th width="15%">operation </th>
					</tr>
                                        </table>
                                        <% 
                                        String group = "";
                                        String status = "";
                                        
                                        for(userattr u: ulist){
                                        %>
                                        <form action="ban_operation" method="post">
                                        <table width="900px;" style="table-layout: fixed;">
					<tr>
						<td width="20%"><%=u.uname%></td>
						<td width="30%"><%=u.email%></td>						
						<td width="20%">
                                                    <% if(u.groupid==3) group = "Administrator";
                                                    else if(u.groupid==2) group = "Moderator";
                                                    else group = "Ordinary";
                                                    %>
                                                    <%=group%>
                                                </td>
                                                <td width="15%"><% if(u.status==0) status = "Normal";
                                                    else status = "isBanned";%>
                                                    <%=status%>
                                                </td>
                                                <td width="15%">
                                                <% if(u.status==0){%>
                                                <input type="hidden" name="username" value=<%=u.uname%>>
                                                <input type="hidden" name="operation" value="ban">
                                                <input type="submit" value="Ban" class="submitButton">
                                                
                                                <%} else {%>
                                                <input type="hidden" name="username" value=<%=u.uname%>>
                                                <input type="hidden" name="operation" value="unban">
                                                <input type="submit" value="Unban" class="submitButton">
                                                <% } %>
                                                </td>
					</tr>                                        
					</table>                      	 
                                        </form>
                                        <% } %>
				</div>
			</div>					
		</div>
		</div>		
	</body>
</html>
